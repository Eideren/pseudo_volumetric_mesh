﻿using UnityEngine;

#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
[RequireComponent(typeof(Camera))]
public class Volume_DepthBuffer : MonoBehaviour
{
	/// <summary>
	/// <br/> The uav index to register our compute buffer at,
	/// <br/> must be the same as the one specified in the shader.
	/// </summary>
	const int RW_REGISTER = 1;
	/// <summary>
	/// <br/> This is overkill but a buffer's stride must be at least
	/// <br/> 4 bytes long, which in this case is used for a 32 bit float.
	/// </summary>
	const int BUFFER_SIZE = 4;
	/// <summary>
	/// <br/> Name of the variable helping with indexing
	/// <br/> our 2D pixel position to the 1D buffer.
	/// </summary>
	const string BUFFERINDEXING_HELPER = "V_BufferWidth";
	/// <summary>
	/// <br/> We can share a single large buffer accross the entire
	/// <br/> render pipeline since camera don't render concurrently
	/// <br/> and each shader's use of the buffer is self contained.
	/// </summary>
	static ComputeBuffer buffer;

	static bool disposed;

	/// <summary>
	/// <br/> Will run once as long as a single instance of this monobehaviour
	/// <br/> is on a camera or if we called this scrit statically.
	/// </summary>
	static Volume_DepthBuffer()
	{
		Camera.onPreRender += PreRender;
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.update += () =>
		{
			if (UnityEditor.EditorApplication.isCompiling)
				Dispose();
		};
		UnityEditor.EditorApplication.playModeStateChanged += (s) =>
		{
			if(s == UnityEditor.PlayModeStateChange.ExitingEditMode)
				Dispose();
		};
		disposed = false;
		#endif
	}
	
	/// <summary>
	/// Prepare the buffer, enlarge it if his size is too small to accomodate the renderer
	/// </summary>
	/// <param name="current"></param>
	static void PreRender(Camera current)
	{
		if(disposed)
			return;
		
		int count = current.pixelWidth * current.pixelHeight;
		// Only resize if the amount of pixels required is
		// larger than our buffer count to avoid pointless buffer resizing
		if (buffer == null || buffer.count < count)
		{
		    if(buffer != null)
				buffer.Dispose();
			buffer = new ComputeBuffer(count, BUFFER_SIZE);
		}
		// Set a value to help accessing the buffer's content in shader
		Shader.SetGlobalInt(BUFFERINDEXING_HELPER, current.pixelWidth);
		
		Graphics.ClearRandomWriteTargets();
		Graphics.SetRandomWriteTarget(RW_REGISTER, buffer);
	}
	

	public static void Dispose()
	{
		if (buffer != null)
		{
			buffer.Dispose();
			buffer = null;
		}
		disposed = true;
	}
	
	public static void Restart()
	{
		Dispose();
		disposed = false;
	}
}
