A very light implementation of a pseudo volumetric mesh ; renders the volume in two passes in under 0.1 ms.
The mesh has a visible density both from outside AND inside the mesh, you can create fog volume by just dropping a mesh of the shape of the volume and placing this material on it.
It doesn't support lighting, you will have to walk through the volume to implement it correctly.
Like any transparent object, it doesn't do well with self-overlap and as such is better suited to concave meshes.

Tested on Unity 2017.3