﻿/*
Here's a resume of how this works
    Draw the back faces
    Save their depth to a buffer
    Draw the front faces
    Force the front faces behind the camera to stay right against the near plane instead.
    In the fragment shader, take the closest depth between the backfaces and the scene depth
    Depth blend between it and our front face' depth.
    Alpha blend based on that.



SUPPORT FOR OLDER APIs AND SHADER MODELS, the only porting issue I see is the RWStructuredBuffer, so most solutions will be about replacing it :
*   Render the first pass's depth to a texture before rednering your scene, use that texture once you render the scene with the second pass.

*   Use two -named- grabpass, one before the first pass and one after it, the first grab is to retrieve the scene color and 
    the second is to retrieve the first pass's output which you will set to depth.
    In the second pass you read the second grab to get the backface depth and instead of outputting alpha directly, remove the blending, output 1 A
    and blend your color with the grabbed scene color based on alpha.

*   Setup MRT, write to it in the first pass and read from it in the second 

*   Change the way it is rendered ; Draw the front faces first normally (don't stick to near clip), set the stencil when the pixel passed, 
    then draw the backfaces only where the stencil didn't pass, now everything occuring inside the backface pass should only render while you are inside the mesh so
    you get the closest depth between the scene depth and the backface depth and blend based on that.
    This method won't be volumetric when looking at it from the outside though.

*/

Shader "VolumetricMesh/Unlit"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_BaseColor ("Base Color", COLOR) = (1, 1, 1, 1)
		_depthBlendFactor ("Depth Blend Factor", float) = 1
	}
	
	// FORCE CONTENT INSIDE ALL PASSES
	CGINCLUDE
        #pragma target 4.5
        #include "UnityCG.cginc"
        
        // Precision modifier
        #define F1 half
        #define F2 half2
        #define F3 half3
        #define F4 half4
        
        #define NEARPLANE_OFFSET 0.001
        
        RWStructuredBuffer<F1> buffer : register(u1);
        int V_BufferWidth;
        #define ToBufferIndex(x,y) (x+y*V_BufferWidth)
        
        // Function pulled from 
        inline F4 ASE_ComputeGrabScreenPos( F4 pos )
        {
            #if UNITY_UV_STARTS_AT_TOP
            F1 scale = -1.0;
            #else
            F1 scale = 1.0;
            #endif
            F4 o = pos;
            o.y = pos.w * 0.5f;
            o.y = ( pos.y - o.y ) * _ProjectionParams.x * scale + o.y;
            return o;
        }
        
        inline void ObjectToClipspace_StickToNear( inout F4 vPos, out F1 depth )
        {
            vPos.xyz = UnityObjectToViewPos(vPos.xyz);
            // Force vertices to stick to screen when behind camera, unity's view forward is negative
            vPos.z = min(vPos.z, -(_ProjectionParams.y + NEARPLANE_OFFSET));
            depth = -vPos.z;
            vPos = UnityViewToClipPos(vPos);
        }
    
	ENDCG
	
	SubShader
	{
		Tags 
		{ 
            "IgnoreProjector"="True"
            "Queue"="Transparent"
		    "RenderType"="Transparent"
		}
		
        ZWrite Off
        
        Pass
        {
            
            Stencil
		    {
		        Ref 0
                Comp always
                Pass IncrSat
                Fail DecrSat
                ZFail IncrSat
		    }
		    
		    
            Cull Front
			ColorMask 0
            
            
            CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			
			struct appdata
			{
				F4 vertex : POSITION;
			};

			struct v2f
			{
				F4 vertex : SV_POSITION;
				F1 depth : TEXCOORD0;
			};
			
			
			v2f vert (appdata v)
			{
				v2f o = (v2f)0;
				o.vertex.xyz = UnityObjectToViewPos( v.vertex );
                o.depth = -o.vertex.z;
				o.vertex = UnityViewToClipPos(o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
                buffer[ToBufferIndex(i.vertex.x, i.vertex.y)] = i.depth;
				return F4(0, 0, 0, 1);
			}
			ENDCG
        }
        
		Pass
		{
		    Stencil
		    {
		        Ref 1
		        // lequal = Triangle overlap rendered, increased flickering
		        // equal = No overlap rendered, reduced flickering
                Comp lequal
                Pass zero
                Fail zero
                ZFail zero
		    }
		    
		
		    Blend SrcAlpha OneMinusSrcAlpha
		    
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
            
			struct appdata
			{
				F4 vertex : POSITION;
				F3 normal : NORMAL;
				F2 uv : TEXCOORD0;
			};

			struct v2f
			{
				F4 vertex : SV_POSITION;
				F3 normal : NORMAL;
				F1 depth : DEPTH;
				
				F2 uv : TEXCOORD0;
			    F4 screenPos : TEXCOORD1;
			};

			sampler2D _MainTex; F4 _MainTex_ST;
            sampler2D _CameraDepthTexture;
			
			F1 _depthBlendFactor;
			F4 _BaseColor;
			
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = v.vertex;
				ObjectToClipspace_StickToNear(/*inout*/o.vertex, /*out*/o.depth);
				o.screenPos = ComputeScreenPos(o.vertex);
				
				o.uv = v.uv;
				o.normal = v.normal;
				
				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
			    F4 screenPos = i.screenPos;
			    screenPos = ASE_ComputeGrabScreenPos(screenPos);
			    F4 screenPosProj = UNITY_PROJ_COORD( screenPos );
			    
			    
			    F1 sceneDepth = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, screenPosProj)));
			    
			    F1 backfacesDepth = buffer[ToBufferIndex(i.vertex.x, i.vertex.y)];
			    backfacesDepth = backfacesDepth < 0 ? sceneDepth : backfacesDepth;
			    
			    F1 closestDepth = sceneDepth < backfacesDepth ? sceneDepth : backfacesDepth;
			    F1 frontDepth = i.depth;
			    F1 depthBlend = saturate((closestDepth - frontDepth) * _depthBlendFactor);
			    
			    
				fixed4 col = tex2D(_MainTex, TRANSFORM_TEX(i.uv, _MainTex)) * _BaseColor;
				F3 colorOut = col.rgb;
				F1 alphaOut = depthBlend;
                
				return F4(colorOut, alphaOut);
			}
			ENDCG
		}
	}
}
